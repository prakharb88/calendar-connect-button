=== Calendar Connect Button ===
Contributors: Prakhar
Tags: prakhar, short code, shortcode, template, stylesheet, url, custom, button, enfold
Requires at least: 2.9.0
Tested up to: 5.1.1
Stable tag: rel_1-00
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html


== Description ==
1) Install the Plugin "Calendar Connect Button"

2) Install "Popup Maker", if not already installed.

3) Go to Tools > Import and Choose WordPress ( Run Importer ) and upload the xml file provided to add the required popup code.

4) Now there would be an option to choose "Calendar Connect Button" in Enfold elements.

5) One you add that element(Calendar Connect Button), we have multiple configurations:

	a) Button Label. -- This is the text that appears on your button.
	
	b) Meeting ID --  Enter the Kloudless Meeting ID here
	
	c) Gravity Form ID -- Enter the ID of Gravity, which is to be linked with Kloudless popup.
		 
	d) Name field ID of gravity form -- Enter the field ID for name field ( as we ned to pass it to Kloudless)
	
	e) Email field ID of gravity form -- Enter the field ID for Email field ( as we ned to pass it to Kloudless)
	
	f) Phone ID of gravity form -- Enter the field ID for Phone field ( as we ned to pass it to Kloudless)
	
	g) Website field ID of gravity form -- Enter the field ID for website field ( as we ned to pass it to Kloudless)
	
	h) Primary Color -- Main elements including the load circle, close X, time button boarder, navigation button main color, and time text.
	
	i) Secondary Color -- Title text color, date text, and hover state buttons.
	
	j) On Primary Color -- Text cursor and entry text for form fields.
	
	h) On Secondary Color -- Preloaded text on invitee information form fields and sub text.
	
	i) On Primary Variant Color -- Selected time text and field titles
	
	j) On Secondary Variant Color -- AM/PM Text
	
	k) Background Color -- Enter the Background Color
	
	l) Button Link? --  We can skip it as button is not made to redirect to a link. It is configured to open a gravity form and then pass the required values to Kloudless for meeting scheduling.


== Changelog ==

* Version 1.0 - Initial release.

== Installation ==

There are 2 ways of installation. If your setup supports it, you can search for the plugin, by name, within your WordPress control panel and install it from there.
Alternatively, you download the .zip file, unzip it, and copy the resultant `url_short_codes` folder to the `wp-content/plugins/` folder of your WordPress instaltion folder.

== Frequently Asked Questions ==
There are no FAQs at this time. Feel free to suggest some!


== License ==
This plugin uses the GPLv3 license.

